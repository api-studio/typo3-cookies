<?php
defined('TYPO3_MODE') or die();

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

ExtensionUtility::registerPlugin(
    'APISTUDIO.Cookieviewhelper',
    'CookieOptIn',
    'Cookie Opt In Form'
);
