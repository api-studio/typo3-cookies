<?php
defined('TYPO3_MODE') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
    'cookieviewhelper',
    'Configuration/TypoScript',
    'Cookiebanner Ajax'
);
