<?php
$EM_CONF['cookieviewhelper'] = [
    'title' => 'Cookieviewhelper',
    'description' => 'Diese Extension fügt einen Viewhelper hinzu. Dieser prüft, ob die entsprechenden Cookies' .
        ' freigegen wurden.',
    'version' => '1.1.1',
    'category' => 'misc',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.4.99',
        ],
    ],
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'author' => 'Daniel Pfeil',
    'author_email' => 'd.pfeil@api-studio.de',
    'author_company' => 'API Studio UG haftungsbeschränkt',
];
