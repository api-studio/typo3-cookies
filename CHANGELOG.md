# Changelog

This project follows the principle of semantic versioning (https://semver.org/)

# Planned

* base design for banner form

# 1.1.1

* add wrapper for accepted
* fix save of reset
* improve HideElementViewHelper to react on the reset function

# 1.1.0

* add reset possibility 

# 1.0.0

* add the viewhelper
* add banner controller
* add cookie service
* add composer package definition
* add typo3 plugin