<?php

namespace APISTUDIO\Cookieviewhelper\Service;

use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

class CookieService
{
    /**
     * @var $fe_user FrontendUserAuthentication
     */
    private $fe_user;

    /**
     * @var $storageKey string
     */
    private $storageKey = 'allowedCookies';

    /**
     * @var $decisionKey string
     */
    private $decisionKey;

    /**
     * CookieService constructor.
     */
    final public function __construct()
    {
        $this->fe_user = $GLOBALS['TSFE']->fe_user;
        $this->decisionKey = sprintf("%sdecision", $this->storageKey);
    }

    /**
     * @param array $allowedCookies
     */
    final public function allowCookies(array $allowedCookies): void
    {
        $this->fe_user->setAndSaveSessionData($this->storageKey, $allowedCookies);
        $this->fe_user->setAndSaveSessionData($this->decisionKey, true);
    }

    /**
     * @param array $requestedCookies
     * @return bool
     */
    final public function isAllowedCookies(array $requestedCookies): bool
    {
        $allowedCookies = $this->fe_user->getSessionData($this->storageKey);
        if (is_array($allowedCookies)) {
            foreach ($requestedCookies as $requestedCookie) {
                if (!in_array($requestedCookie, $allowedCookies, true)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    final public function decisionExists(): bool
    {
        return ($this->fe_user->getSessionData($this->decisionKey)) ? true : false;
    }

    public function reset()
    {
        $this->fe_user->setAndSaveSessionData($this->decisionKey, false);
    }
}
