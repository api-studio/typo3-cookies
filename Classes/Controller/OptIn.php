<?php

namespace APISTUDIO\Cookieviewhelper\Controller;

use APISTUDIO\Cookieviewhelper\Service\CookieService;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class OptIn extends ActionController
{

    /**
     * @var CookieService
     */
    private $cookieService;

    /**
     * OptIn constructor.
     * @param CookieService $cookieService
     */
    public function __construct(CookieService $cookieService = null)
    {
        $this->cookieService = ($cookieService) ? $cookieService : new CookieService();
    }


    public function formAction(): void
    {
        $decisionExist = $this->cookieService->decisionExists();

        if (!$decisionExist) {
            $this->cookieService->isAllowedCookies([]);
        }

        $this->view->assign("decisionExist", $decisionExist);
    }

    public function updateAction(): void
    {
        $cookies = $this->request->getArgument('selectedCookies');
        if (!is_array($cookies)) {
            $cookies = [];
        }

        (new CookieService())->allowCookies($cookies);

        header("Location: {$this->settings['redirectPage']}", true, 302);
        die();
    }

    public function resetAction(): void
    {
        $this->cookieService->reset();

        header("Location: {$this->settings['redirectPage']}", true, 302);
        die();
    }
}
