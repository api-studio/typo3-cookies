<?php

namespace APISTUDIO\Cookieviewhelper\ViewHelpers;

use APISTUDIO\Cookieviewhelper\Service\CookieService;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class HideElementViewHelper extends AbstractConditionViewHelper
{
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument(
            'cookieTitle',
            'string',
            'The name of the cookie type which should be checked.',
            true
        );
    }

    /**
     * Static method which can be overridden by subclasses. If a subclass
     * requires a different (or faster) decision then this method is the one
     * to override and implement.
     *
     * @param array $arguments
     * @param RenderingContextInterface $renderingContext
     * @return bool
     */
    public static function verdict(array $arguments, RenderingContextInterface $renderingContext): bool
    {
        $cookieService = new CookieService();
        if ($cookieService->decisionExists()) {
            return $cookieService->isAllowedCookies([$arguments['cookieTitle']]);
        }
        return false;
    }
}
