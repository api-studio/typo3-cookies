<?php
defined('TYPO3_MODE') || die('Access denied.');

use APISTUDIO\Cookieviewhelper\Controller\OptIn;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

ExtensionUtility::configurePlugin(
    'APISTUDIO.Cookieviewhelper',
    'CookieOptIn',
    [
        OptIn::class => 'form, update, reset',
    ],
    [
        OptIn::class => 'form, update, reset',
    ]
);
